#!/usr/bin/env perl

use warnings;
use strict;

use Getopt::Long;
use Pod::Usage;
use autodie;
use Data::Dumper;
use JSON;
use File::Slurp qw(read_file);

my %opt;

{
    local $SIG{__WARN__};
    my $ok = eval {
        GetOptions(
            \%opt, qw(
                help
                n
                username=s
                password=s
                api_ident=s
                url=s
                digest
                path=s
                json=s
                zql=s
                binary
                v
                query=s
                api=s
                )
        );
    };
    if (!$ok) {
        die($@);
    }
}

pod2usage(0) if ($opt{help});
pod2usage(1) if !scalar keys %opt;

my $missing = 0;
foreach (qw(url)) {
    if (!defined $opt{$_}) {
        warn "Missing option: $_";
        $missing = 1;
    }
}
pod2usage(1) if $missing;

my $zs;
if ($opt{api} eq '1') {
    use Zaaksysteem::JSON::Client::V1;
    $zs = Zaaksysteem::JSON::Client::V1->new(
        user     => $opt{username},
        password => $opt{password},
        url      => $opt{url},
        binary   => $opt{binary},
        verbose  => $opt{v},
        api_ident => $opt{api_ident},
    );
}
else {
    use Zaaksysteem::JSON::Client;
    $zs = Zaaksysteem::JSON::Client->new(
        user     => $opt{username},
        password => $opt{password},
        url      => $opt{url},
        binary   => $opt{binary},
        verbose  => $opt{v},
    );
}


$zs->login;

if ($opt{json}) {
    my $json = read_file($opt{json});
    print Dumper $zs->post($opt{path}, { object => $json });
}
elsif ($opt{path} && $opt{zql}) {
    print Dumper $zs->search($opt{path}, $opt{zql});
}
elsif ($opt{path}) {
    print Dumper $zs->get($opt{path});
    if ($zs->can('next')) {
        while (my $json = $zs->next()) {
            print Dumper $json;
        }
    }
}
else {
    warn "Unable to process your request, it is us, not you";
    pod2usage(1);
}


__END__

=head1 NAME

zs-json.pl - A JSON client for Zaaksysteem

=head1 SYNOPSIS

zs-json.pl OPTIONS

=head2 OPTIONS

=over

=item help

This help

=item url

The base URL of Zaaksysteem, required

=item username

The username you want to use to login with

=item password

The password

=item path

The path you want to throw your request at

=item zql

ZQL statement

=item digest

Boolean, to use HTTP Digest authentication. Currently not supported.

=back

=head1 COPYRIGHT and LICENSE

Copyright (c) 2014, Mintlab B.V. All rights reserved

