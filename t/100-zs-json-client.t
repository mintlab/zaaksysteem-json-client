#! perl
use strict;
use warnings;

use Test::More;
use Test::Exception;

use File::Spec::Functions qw(catfile);
use JSON;
use autodie;

use Zaaksysteem::JSON::Client;
use HTTP::Request;
use Test::LWP::UserAgent;

{
    my $username = "test";
    my $password = "test";
    my $url      = "http://testsuite";
    my @keys     = qw(at comment next num_rows prev result rows status_code);

    my %data     = map { $_ => "key $_" } @keys;
    my @result;

    my $ua = Test::LWP::UserAgent->new;

    my $json = encode_json(\%data);

    my $zs = Zaaksysteem::JSON::Client->new(
        user     => $username,
        password => $password,
        url      => $url,
        ua       => $ua,
    );
    isa_ok($zs, "Zaaksysteem::JSON::Client");

    is($zs->_generate_uri("meuk", ab => 'meuk'), "$url/meuk?ab=meuk");


#    TODO: {
#        local $TODO = "Breakage";
#        $ua->map_response(
#            qr{auth/login},
#            HTTP::Response->new(
#                '200', 'OK', ['Content-Type' => 'application/json'], $json
#            )
#        );
#        ok $zs->login, "Login succesfull";
#    }

    $ua->unmap_all();
    $ua->map_response(
        qr{auth/login},
        HTTP::Response->new(
            '500', 'ERR', ['Content-Type' => 'application/json'], $json
        )
    );
    throws_ok(
        sub {
            $zs->login;
        },
        qr/^Unable to login/,
        "Unable to login"
    );

    $ua->map_response(
        qr{api/object/INVALID},
        HTTP::Response->new(
            '500', 'ERR', ['Content-Type' => 'application/json'], $json
        )
    );

    $ua->map_response(
        qr{api/object},
        HTTP::Response->new(
            '200', 'OK', ['Content-Type' => 'application/json'], $json
        )
    );

    @result = $zs->search('/api/object', "SELECT {} FROM domain");
    is(@result, 1, "Got SEARCH result");

    @result = $zs->get('/api/object', zql => "SELECT {} FROM domain");
    is(@result, 1, "Got GET result");

    throws_ok(
        sub {
            $zs->get('/api/object/INVALID');
        },
        qr/500/,
        "Invalid request!"
    );

    @result = $zs->post('/api/object', \%data);
    is(@result, 1, "Got POST result");

    $ua->unmap_all();

    $ua->map_response(
        qr{api/object},
        HTTP::Response->new(
            '200', 'OK', ['Content-Type' => 'text/html'], $json
        )
    );
    throws_ok(
        sub {
            $zs->search('/api/object', "SELECT {} FROM domain");
        },
        qr/^Not a JSON response/,
        "Call did not return JSON"
    );

    $ua->unmap_all();

    delete $data{result};
    $json = encode_json(\%data);

    $ua->map_response(
        qr{api/object},
        HTTP::Response->new(
            '200', 'OK', ['Content-Type' => 'application/json'], $json
        )
    );

    throws_ok(
        sub {
            $zs->search('/api/object', "SELECT {} FROM domain");
        },
        qr/^Invalid JSON response, expected keys are not available/,
        "Call did not return JSON"
    );
}

done_testing;
