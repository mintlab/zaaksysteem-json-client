NAME
    zs-json.pl - A JSON client for Zaaksysteem

SYNOPSIS
    zs-json.pl OPTIONS

  OPTIONS
    help
        This help

    url The base URL of Zaaksysteem, required

    username
        The username you want to use to login with

    password
        The password

    path
        The path you want to throw your request at

    zql ZQL statement

    digest
        Boolean, to use HTTP Digest authentication. Currently not supported.

COPYRIGHT and LICENSE
    Copyright (c) 2014, Mintlab B.V. All rights reserved

